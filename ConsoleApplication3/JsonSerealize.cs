﻿using System.Linq;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Collections;
using System;
using Newtonsoft.Json;

namespace ConsoleApplication3
{
    public class JsonSerealize<T>
    {

        private Hashtable serializers = new Hashtable();

        private object GetSerializer(Type key)
        {
            if (serializers.ContainsKey(key))
            {
                return (object)serializers[key];
            }
            else
            {
                serializers.Add(key, new DataContractJsonSerializer(key));
                return (object)serializers[key];
            }
        }
        public T Deserialize<T>(string serializedData)
        {
            return JsonConvert.DeserializeObject<T>(serializedData);
        }
        public string Serialize<T>(T data)
        {
            return JsonConvert.SerializeObject(data);
        }
        public static Output MakeOutputFromInput(Input someInput)
        {
            Output Res = new Output();
            Res.SumResult = someInput.Sums.Sum() * someInput.K;
            Res.MulResult = someInput.Muls.Aggregate((acc, i) => acc * i);
            someInput.Sums = someInput.Sums.Select(i => (decimal)i + 0.0M).ToArray();
            Res.SortedInputs = someInput.Sums.Concat(someInput.Muls.Select(i => (decimal)i + 0.0M)).OrderBy(x => x).ToArray();
            return Res;
        }

    }
}
