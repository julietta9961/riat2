﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication3
{
    interface ItoDo
    {
       Task<T> GET<T>( string requestUri);
       void POST<T>(T PostArray, string requestUri);
        void Connection(string requestUri);

    }
}
