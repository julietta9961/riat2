﻿using System;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using System.Text;

namespace ConsoleApplication3
{
    public class MyServerClient :ItoDo
    {
        public HttpWebResponse MyResultConnect;
        public HttpWebRequest MyConnect;
        private string ServerAdress;
        public MyServerClient(string str)
        {
            ServerAdress = "http://127.0.0.1:" + str;
        }

        async Task<T> ItoDo.GET<T>( string requestUri)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(ServerAdress);
            ServicePoint sp = MyConnect.ServicePoint;
            sp.Expect100Continue = false;
            var response = await client.GetByteArrayAsync(ServerAdress + requestUri );
            return (T)Convert.ChangeType(response, typeof(T)) ;
        }

        void ItoDo.POST<T>(T GetData, string requestUri)
        {
            Converter Convertt = new Converter();
            byte[] PostArray = Convertt.Convertt<T>(GetData);
            HttpClient Client = new HttpClient();
            Client.BaseAddress = new Uri(ServerAdress);
            ServicePoint sp = MyConnect.ServicePoint;
            sp.Expect100Continue = false;
            HttpContent content = new ByteArrayContent(PostArray);
            var response = Client.PostAsync(requestUri, content);
            HttpContent take = response.Result.Content;
            string responseFromServer = take.ReadAsStringAsync().Result;
        }
         void ItoDo.Connection( string requestUri)
        {
            HttpWebResponse MyResultConnect;
            HttpWebRequest MyConnect;
            do
            {
                MyConnect = (HttpWebRequest)WebRequest.Create(ServerAdress + requestUri);
                MyConnect.Method = "GET";
                MyResultConnect = (HttpWebResponse)MyConnect.GetResponse();
            }
            while (MyResultConnect.StatusCode != HttpStatusCode.OK);
            MyResultConnect.Close();
            this.MyConnect = MyConnect;
            this.MyResultConnect = MyResultConnect;
        }
    }
}
