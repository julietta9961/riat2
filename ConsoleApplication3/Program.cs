﻿using System;
using System.Text;

namespace ConsoleApplication3
{

    class Program
    {
        static void Main(string[] args)
        {
           
            MyServerClient Client = new MyServerClient(Console.ReadLine());
            ItoDo todo = (ItoDo)Client;
            todo.Connection("/Ping");
            var GetData = todo.GET<byte[]>("/GetInputData");
            todo.POST( GetData.Result , "/WriteAnswer");
        }
    }
}
